package sbp.collections;


import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class CustomArrayImplTest {

    /**
     * Check constructor class {@link CustomArrayImpl}
     */
    @Test
    void CustomArrayImpl_Test() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        assertInstanceOf(CustomArrayImpl.class, customArray);
        assertNotNull(customArray);
        assertEquals(0, customArray.size());
        assertEquals(0, customArray.getCapacity());

    }

    /**
     * Check constructor class with capacity {@link CustomArrayImpl}
     */
    @Test
    void CustomArrayImplCapacity_Test() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>(10);
        assertInstanceOf(CustomArrayImpl.class, customArray);
        assertNotNull(customArray);
        assertEquals(0, customArray.size());
        assertEquals(10, customArray.getCapacity());
    }

    /**
     * Check constructor class witch {@link CustomArrayImpl}
     */
    @Test
    void CustomArrayImplClass_Test() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("a");
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>(arrayList);
        assertInstanceOf(CustomArrayImpl.class, customArray);
        assertNotNull(customArray);
        assertEquals(arrayList.size(), customArray.size());
        assertFalse(customArray.isEmpty());
        assertEquals("a", customArray.get(0));

    }

    /**
     * Check method {@link CustomArrayImpl#size()}
     */
    @Test
    void size_Test() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        assertEquals(0, customArray.size());
        customArray.add("a");
        assertEquals(1, customArray.size());
    }

    /**
     * Check method {@link CustomArrayImpl#isEmpty()}
     */
    @Test
    void isEmpty_Test() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        assertTrue(customArray.isEmpty());
        customArray.add("a");
        assertFalse( customArray.isEmpty());
    }

    /**
     * Check method {@link CustomArrayImpl#add(Object)}
     */
    @Test
    void add_Test() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        customArray.add("a");
        assertEquals("a", customArray.get(0));
    }

    /**
     * Check method {@link CustomArrayImpl#addAll(Object[])}
     */
    @Test
    void addAllArray_Test() {
        CustomArrayImpl<String> firstArray = new CustomArrayImpl<>();
        firstArray.add("a");
        String[] lastArray = new String[6];
        lastArray[0] = "b";
        firstArray.addAll(lastArray);
        String string = "[a, b]";
        assertEquals(string, firstArray.toString());
    }

    /**
     * Check method {@link CustomArrayImpl#addAll(Collection)}
     */
    @Test
    void AddAllCollection_Test() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        customArray.add("a");
        customArray.add("b");
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("c");
        arrayList.add("d");
        assertThrows(IllegalArgumentException.class, () -> customArray.addAll((Collection<String>) null));
        customArray.addAll(arrayList);
        String string = "[a, b, c, d]";
        assertEquals(string, customArray.toString());
    }

    /**
     * Check method {@link CustomArrayImpl#addAll(int, Object[])}
     */
    @Test
    void AddAllInsert_Test() {
        CustomArrayImpl<String> firstArray = new CustomArrayImpl<>();
        firstArray.add("a");
        firstArray.add("b");
        String[] lastArray = new String[6];
        lastArray[0] = "c";
        assertThrows(ArrayIndexOutOfBoundsException.class,() -> firstArray.addAll(10, lastArray));
        assertThrows(IllegalArgumentException.class,() -> firstArray.addAll(1, null));
        firstArray.addAll(1, lastArray);
        String string = "[a, c]";
        assertEquals(string, firstArray.toString());
    }

    /**
     * Check method {@link CustomArrayImpl#get(int)}
     *
     */
    @Test
    void get_Test() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        customArray.add("a");
        assertEquals("a", customArray.get(0));
        assertThrows(ArrayIndexOutOfBoundsException.class,() -> customArray.get(10));
    }

    /**
     * Check method {@link CustomArrayImpl#set(int, Object)}
     */
    @Test
    void set_Test() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        customArray.add("a");
        customArray.set(0, "h");
        assertEquals("h", customArray.get(0));
        assertThrows(ArrayIndexOutOfBoundsException.class,() -> customArray.set(10,"One"));
    }

    /**
     * Check method {@link CustomArrayImpl#remove(int)}
     */
    @Test
    void removeInt_Test() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        customArray.add("a");
        customArray.remove(0);
        assertEquals(0,customArray.size());
        assertThrows(ArrayIndexOutOfBoundsException.class,() -> customArray.remove(10));
    }

    /**
     * Check method {@link CustomArrayImpl#remove(Object)}
     */
    @Test
    void RemoveItem_Test() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        customArray.add("a");
        assertFalse(customArray.remove("b"));
        assertTrue(customArray.remove("a"));
    }

    /**
     * Check method {@link CustomArrayImpl#contains(Object)}
     */
    @Test
    void contains_Test() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        customArray.add("a");
        customArray.add("b");
        customArray.add("c");
        assertTrue(customArray.contains("b"));
        assertFalse(customArray.contains("H"));
    }

    /**
     * Check method {@link CustomArrayImpl#indexOf(Object)}
     */
    @Test
    void indexOf_Test() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        customArray.add("a");
        customArray.add("b");
        customArray.add("c");
        assertEquals(1, customArray.indexOf("b"));
        assertEquals(-1, customArray.indexOf("H"));
    }

    /**
     * Check method {@link CustomArrayImpl#ensureCapacity(int)}
     */
    @Test
    void ensureCapacity_Test() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>(8);
        customArray.ensureCapacity(10);
        assertEquals(10,customArray.getCapacity());
        customArray.ensureCapacity(4);
        assertEquals(4,customArray.getCapacity());
    }

    /**
     * Check method {@link CustomArrayImpl#getCapacity()}
     */
    @Test
    void getCapacity_Test() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>(8);
        assertEquals(8,customArray.getCapacity());
    }

    /**
     * Check method {@link CustomArrayImpl#reverse()}
     */
    @Test
    void reverse_Test() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        customArray.add("a");
        customArray.add("b");
        customArray.add("c");
        customArray.reverse();
        String stringReverse = "[c, b, a]";
        assertEquals(customArray.toString(), stringReverse);
    }

    /**
     * Check method {@link CustomArrayImpl#toArray()}
     */
    @Test
    void toArray_Test() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        customArray.add("a");
        customArray.add("b");
        customArray.add("c");
        assertNotNull(customArray.toArray());
        assertInstanceOf(Object.class,customArray.toArray());
    }

    /**
     * Check method {@link CustomArrayImpl#equals(Object)}
     */
    @Test
    void equals_Test() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        customArray.add("a");
        customArray.add("b");
        customArray.add("c");
        CustomArrayImpl<String> twoArray = new CustomArrayImpl<>();
        twoArray.add("a");
        twoArray.add("b");
        twoArray.add("c");
        assertEquals(customArray, twoArray);
        twoArray.set(0, "r");
        assertNotEquals(customArray, twoArray);
    }

    /**
     * Check method {@link CustomArrayImpl#hashCode()}
     */
    @Test
    void hashCode_Test() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        customArray.add("a");
        customArray.add("b");
        customArray.add("c");
        Integer hash = customArray.hashCode();
        assertNotNull(hash);
    }

    /**
     * Check method {@link CustomArrayImpl#toString()}
     */
    @Test
    void testToString_Test() {
        CustomArrayImpl<String> customArray = new CustomArrayImpl<>();
        customArray.add("a");
        customArray.add("b");
        customArray.add("c");
        String string = "[a, b, c]";
        assertEquals(customArray.toString(), string);
    }
}