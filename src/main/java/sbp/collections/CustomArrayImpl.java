package sbp.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;


/**
 * Resizable-array implementation of the {@code CustomArray} interface.  Implements
 * all optional CustomArray operations, and permits all elements, including
 * {@code null}.
 * this class provides methods to manipulate the size of the array that is
 * used internally to store the array.
 *
 * @param <T> - the type of elements in this Array
 * @author Denis Medyantsev *
 * @see Collection
 * @see Arrays
 * @see ArrayList
 */

public class CustomArrayImpl<T> implements CustomArray<T> {

    /**
     * Shared empty array instance used for empty instances.
     */
    private static final Object[] EMPTY_ELEMENT_DATA = {};

    /**
     * Shared empty array instance used for default sized empty instances.
     * We distinguish this from EMPTY_ELEMENT_DATA to know how much to
     * inflate when first element is added.
     */
    private static final Object[] DEFAULT_CAPACITY_EMPTY_ELEMENT_DATA = {};

    /**
     * The array buffer into which the elements of the Array are stored.
     * The capacity of the Array is the length of this array buffer.
     * Any empty Array with elementData == DEFAULT_CAPACITY_EMPTY_ELEMENT_DATA
     * will be expanded to DEFAULT_CAPACITY when the first element is added.
     */
    transient T[] elementData;

    /**
     * The size of the Array (the number of elements it contains).
     */
    private int size;

    /**
     * The capacity of the Array.
     */
    private int capacity;

    /**
     * Constructs an empty array with the specified initial capacity.
     *
     * @param capacity – the initial capacity of the array
     * @throws ArrayIndexOutOfBoundsException if index is out of bounds
     */
    public CustomArrayImpl(int capacity) {
        if (capacity > 0) {
            this.elementData = (T[]) new Object[capacity];
            this.capacity = capacity;
        } else if (capacity == 0) {
            this.elementData = (T[]) EMPTY_ELEMENT_DATA;
        } else {
            throw new ArrayIndexOutOfBoundsException("Illegal Capacity: " + capacity);
        }
    }

    /**
     * Constructs array containing the elements of the specified collection.
     *
     * @param c – the collection whose elements are to be placed into this array
     */
    public CustomArrayImpl(Collection<? extends T> c) {
        Object[] a = c.toArray();
        if ((size = a.length) != 0) {
            this.elementData = (T[]) Arrays.copyOf(a, size, Object[].class);
            this.capacity = size;
        } else {
            this.elementData = (T[]) EMPTY_ELEMENT_DATA;
        }
    }

    /**
     * Constructs an empty array with an initial capacity of ten.
     */
    public CustomArrayImpl() {
        this.elementData = (T[]) DEFAULT_CAPACITY_EMPTY_ELEMENT_DATA;
    }

    /**
     * Returns the number of elements in this array.
     *
     * @return size - the number of elements in this array
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Returns true if this array contains no elements.
     *
     * @return true - if this array contains no elements
     */
    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Appends the specified element to the end of this Array.
     *
     * @param item – element to be appended to this array
     * @return true (as specified by added)
     */
    @Override
    public boolean add(T item) {
        T[] tempArray = (T[]) new Object[size + 1];
        System.arraycopy(elementData, 0, tempArray, 0, size);
        tempArray[size] = item;
        elementData = tempArray;
        ++size;
        if (capacity < size) capacity = size;
        return true;

    }

    /**
     * Add all items.
     *
     * @param items - another array to add to the array
     * @return true if item was added another array
     * @throws IllegalArgumentException if parameter items is null
     */
    @Override
    public boolean addAll(T[] items) {
        if (items == null) {
            throw new IllegalArgumentException("Parameter is null");
        }
        int count = 0;
        for (T item : items) {
            if (item != null) {
                count++;
            }
        }
        T[] temp = (T[]) new Object[count];

        count = 0;
        for (T item : items) {
            if (item != null) {
                temp[count++] = item;
            }
        }
        int newSize = elementData.length + temp.length;
        T[] tempArray = (T[]) new Object[newSize];
        System.arraycopy(elementData, 0, tempArray, 0, elementData.length);
        System.arraycopy(temp, 0, tempArray, elementData.length, temp.length);
        elementData = tempArray;
        size = newSize;
        if (capacity < size) capacity = size;
        return true;
    }

    /**
     * Add all items.
     *
     * @param items - another Collection to add to the array
     * @return true if item was added another Collection
     * @throws IllegalArgumentException if parameter items is null
     */
    @Override
    public boolean addAll(Collection<T> items) {
        if (items == null) {
            throw new IllegalArgumentException("Parameter is null");
        }
        return addAll((T[]) items.toArray());
    }

    /**
     * Add items to current place in array.
     *
     * @param index - index
     * @param items - items for insert
     * @return true if item was added
     * @throws ArrayIndexOutOfBoundsException if index is out of bounds
     * @throws IllegalArgumentException       if parameter items is null
     */
    @Override
    public boolean addAll(int index, T[] items) {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException("Index out of bounds");
        }
        if (items == null) {
            throw new IllegalArgumentException("Parameter is null");
        }
        int count = 0;
        for (T item : items) {
            if (item != null) {
                count++;
            }
        }
        T[] temp = (T[]) new Object[count];

        count = 0;
        for (T item : items) {
            if (item != null) {
                temp[count++] = item;
            }
        }
        int newSize = index + temp.length;
        T[] tempArray = (T[]) new Object[newSize];
        System.arraycopy(elementData, 0, tempArray, 0, index);
        System.arraycopy(temp, 0, tempArray, index, temp.length);
        elementData = tempArray;
        size = newSize;
        if (capacity < size) capacity = size;
        return true;
    }

    /**
     * Get item by index.
     *
     * @param index - index
     * @return element with an index in the array
     * @throws ArrayIndexOutOfBoundsException if index is out of bounds
     */
    @Override
    public T get(int index) {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException("Index out of bounds");
        }
        return elementData[index];
    }

    /**
     * Set item by index.
     *
     * @param index - index
     * @param item - new value
     * @return old value
     * @throws ArrayIndexOutOfBoundsException if index is out of bounds
     */
    @Override
    public T set(int index, T item) {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException("Index out of bounds");
        }
        T oldValue = elementData[index];
        elementData[index] = item;
        return oldValue;
    }

    /**
     * Remove item by index.
     *
     * @param index - index
     * @throws ArrayIndexOutOfBoundsException if index is out of bounds
     */
    @Override
    public void remove(int index) {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException("Index out of bounds");
        }
        T[] tempArray = (T[]) new Object[size - 1];
        System.arraycopy(elementData, 0, tempArray, 0, index);
        System.arraycopy(elementData, index + 1, tempArray, index, (elementData.length - 1) - index);
        elementData = tempArray;
        size--;
    }

    /**
     * Remove item by value. Remove first item occurrence.
     *
     * @param item - item
     * @return true if item was removed or false if item not removed
     */
    @Override
    public boolean remove(T item) {
        for (int i = 0; i < elementData.length; ++i) {
            if (elementData[i].equals(item)) {
                T[] tempArray = (T[]) new Object[size - 1];
                System.arraycopy(elementData, 0, tempArray, 0, i);
                System.arraycopy(elementData, i + 1, tempArray, i, (size - 1) - i);
                elementData = tempArray;
                size--;
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if item exists.
     *
     * @param item - item
     * @return true if array exist item or false if array not exist item
     */
    @Override
    public boolean contains(T item) {
        for (T param : elementData) {
            if (param.equals(item)) return true;
        }
        return false;
    }

    /**
     * Index of item.
     *
     * @param item - item
     * @return index of element or -1 of array doesn't contain element
     */
    @Override
    public int indexOf(T item) {
        for (int i = 0; i < elementData.length; ++i) {
            if (elementData[i].equals(item)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Current capacity to store new elements if needed.
     *
     * @param newElementsCount - new elements count
     */
    @Override
    public void ensureCapacity(int newElementsCount) {
        if (newElementsCount > 0) {
            T[] tempArray = (T[]) new Object[newElementsCount];
            if (newElementsCount < size) {
                System.arraycopy(elementData, 0, tempArray, 0, newElementsCount);
                size = newElementsCount;
            } else {
                System.arraycopy(elementData, 0, tempArray, 0, size);
            }
            this.elementData = tempArray;
            this.capacity = newElementsCount;
        } else if (newElementsCount == 0) {
            this.elementData = (T[]) EMPTY_ELEMENT_DATA;
            this.capacity = 0;
            this.size = 0;
        } else {
            throw new IllegalArgumentException("Illegal new Capacity: " +
                    capacity);
        }
    }

    /**
     * Get current capacity.
     *
     * @return result contains capacitance array
     */
    @Override
    public int getCapacity() {
        return capacity;
    }

    /**
     * Reverse array.
     */
    @Override
    public void reverse() {
        T[] tempArray = (T[]) new Object[size];
        for (int i = 0; i < size; ++i){
           tempArray[i] = elementData[(size-1)-i];
        }
        elementData = tempArray;
    }

    /**
     * Get copy of current array.
     *
     * @return result contains Object array
     */
    @Override
    public Object[] toArray() {
        Object[] tempArray = new Object[size];
        System.arraycopy(elementData, 0, tempArray, 0, size);
        return tempArray;

    }

    /**
     * Check for equality.
     *
     * @param o - Objet to check for equality
     * @return true if checked equality
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CustomArrayImpl)) return false;
        CustomArrayImpl<?> that = (CustomArrayImpl<?>) o;
        return size == that.size && capacity == that.capacity && Arrays.equals(elementData, that.elementData);
    }

    /**
     * Generation hash.
     *
     * @return result contains generated the hash code
     */
    @Override
    public int hashCode() {
        int result = Objects.hash(size, capacity);
        result = 31 * result + Arrays.hashCode(elementData);
        return result;
    }

    /**
     * Print toString array.
     *
     * @return result contains string representation array
     */
    @Override
    public String toString() {
        return  Arrays.toString(elementData);
    }
}
